import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import Ember from 'ember';

moduleForComponent('change-list', 'Integration | Component | change list', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  const model = new Ember.A();
  const fields = new Ember.A();
  this.set('model', model);
  this.set('fields', fields);

  this.render(hbs`{{change-list model=model label='brol' fields=fields}}`);

  const result = this.$().text().trim().replace(/ *\n*/g, '');
  assert.ok(result.match(/selected:/));
  assert.ok(result.match(/Go/));
  assert.ok(result.match(/Brols/));
});
