import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('form-markdown', 'Integration | Component | form markdown', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{form-markdown}}`);

  assert.equal(this.$().text().trim().replace(/ *\n*/g, '').substring(0,4), 'Edit');
  assert.equal(this.$().text().trim().replace(/ *\n*/g, '').substring(6), 'View');
});
