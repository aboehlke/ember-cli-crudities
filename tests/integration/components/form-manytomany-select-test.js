import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('form-manytomany-select', 'Integration | Component | form manytomany select', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  const model = new Ember.Object({
    related: new Ember.A()
  });
  const extra = {
    property_path: 'related'
  };
  this.set('model', model);
  this.set('extra', extra);
  this.render(hbs`{{form-manytomany-select model=model extra=extra}}`);

  assert.equal(this.$().text().trim(), '');
});
