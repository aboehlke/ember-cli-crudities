import Ember from 'ember';
import ConfigurableLoaderMixin from 'ember-cli-crudities/mixins/configurable-loader';
import { module, test } from 'qunit';

module('Unit | Mixin | configurable loader');

// Replace this with your real tests.
test('it works', function(assert) {
  const ConfigurableLoaderObject = Ember.Object.extend(ConfigurableLoaderMixin);
  const subject = ConfigurableLoaderObject.create();
  assert.ok(subject);
});
