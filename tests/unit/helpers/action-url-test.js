import { actionUrl } from 'dummy/helpers/action-url';
import { module, test } from 'qunit';

module('Unit | Helper | action url');

// Replace this with your real tests.
test('it works', function(assert) {
  const result = actionUrl(['/blah/:id/brol', 42]);
  assert.equal(result, '/blah/42/brol');
});
