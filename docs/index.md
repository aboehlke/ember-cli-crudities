# Ember-cli-crudities

:warning: This package is pre-alpha and under heavy development.

**ember-cli-crudities** is a form and editable list builder that works from json config
which can be either statically or dynamically loaded.

## Installation

`ember install ember-cli-crudities`

## Usage

### Application structure

This may change in the future but at the moment, ember-cli-crudities expects both your
ember models and your api to be structured around "applications". This should sound familiar to you if you're already using RoR, Django or Phoenix.

Example model and api structure:

```
## ember application structure

- app
  - models
    - sales
      - product.js
      - category.js
    - accounting
      - invoice.js
      - invoiceline.js

## api structure

- http://localhost/api/
  - sales/products
  - sales/categories
  - accounting/invoices
  - accounting/invoicelines
```

### Components

**ember-cli-crudities** provides a set of component to make building forms and editable
lists around CRUD API's easier.
By default, all components are pre-configured to be compatible with bootstrap class-names
but, since you can specify class-names for each component and widget, it shouldn't be too
much trouble to use it with another css setup.

There are 4 top-level components in **ember-cli-crudities**:

- [app-dashboard](./app-dashboard.md)
- [change-list](./change-list.md)
- [change-form](./change-form.md)
- [app-breadcrumbs](./app-breadcrumbs.md)

Both `change-list` and `change-form` make heavy usage of a set of
[widget components](./widgets.md).

## Demo application

You can see a demo application running at
[https://djembersample.pythonanywhere.com/](https://djembersample.pythonanywhere.com/).
