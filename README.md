# Ember-cli-crudities

**ember-cli-crudities** is a form and editable list builder that works from json config
which can be either statically or dynamically loaded.

:warning: This package is still in alpha stage and under heavy development.

## 0.2.x reached

V0.2.x has been reached, what does it mean for you? It means that all the core
functionalities that we wanted to have in 1.0.0 are there and working.

The scope of the 0.2.x series will be to add missing widgets and improve general UI like
adding spinners on save/delete buttons and so on.

## What's next?

- v 0.3.x series: adding meaningful tests
- v 0.4.x series: adding the css into the project
- v 1.0.x series: when we'll have reached an "acceptable" amount of tests
- v 1.1.x series: porting this addon to an engine
- v 1.2.x series: adding material design theme

## Installation

`ember install ember-cli-crudities`

## Full Documentation

Available on [ReadTheDocs](https://ember-cli-crudities.readthedocs.io)

## Contributors

* [Emmanuelle Delescolle](https://bitbucket.org/emma_nuelle/)
* [Karl Ranna](https://bitbucket.org/erkarl/)
