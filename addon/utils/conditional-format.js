import evaluate from './evaluator';

export default function conditionalFormat(model, conditions) {
  let prop;
  let propval;
  let i;

  for (prop in conditions) {
    if (conditions.hasOwnProperty(prop)) {
      for (i=0; i<conditions[prop].length; i++) {
        propval = model.get(conditions[prop][i].property_path);
        if (evaluate(conditions[prop][i], propval)) {
          return prop;
        }
      }
    }
  }
  return '';
}
