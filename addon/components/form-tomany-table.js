import Ember from 'ember';
import ToManyStack from './form-tomany-stack';
import layout from '../templates/components/form-tomany-table';
import withCustomActionsMixin from '../mixins/with-custom-actions';

export default ToManyStack.extend(withCustomActionsMixin, {
  layout,
  classNames: ['tomany-table'],

  actionColumns: Ember.computed('field.extra.customActions', 'field.extra.customActions.length', 'field.extra.allowOpen', function() {
    const rv = parseInt(this.get('custom_actions.length') ? this.get('custom_actions.length') : 0) +
      parseInt(this.get('field.extra.allowOpen') ? 1 : 0);
    return rv;
  }),

  column_count: Ember.computed('field.fields.length', 'actionColumns', function() {
    const readonly = this.get('readonly');
    const rv = parseInt(this.get('field.fields.length')) +
      parseInt(readonly !== true ? 1 : 0) +
      parseInt(!readonly && this.get('field.extra.sortableBy') ? 1 : 0) +
      parseInt(this.get('actionColumns'));
    return rv;
  }),
});
