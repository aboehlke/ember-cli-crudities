import Ember from 'ember';
import layout from '../templates/components/form-tabset';

export default Ember.Component.extend({
  layout,
  tagName: null,

  activeIndex: 0,

  /** For compatibility with change-form actions **/
  fields: Ember.computed('tabs', 'tabs.length', 'tabs.@each.fields', function() {
    const rv = new Ember.A();
    const tabs = this.get('tabs');
    tabs.forEach((tab) => {
      tab.get('fields').map((field) => {
        rv.pushObject(field);
      });
    });
    return rv;
  }),

  actions: {
    activateTab(index) {
      this.set('activeIndex', index);
    }
  }
});
