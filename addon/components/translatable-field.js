import Ember from 'ember';
import layout from '../templates/components/translatable-field';

export default Ember.Component.extend({
  layout,
  langs: Ember.computed('languages', 'languages.length', 'languages[]', function() {
    const languages = this.get('languages');
    const rv = new Ember.A();
    if (languages === undefined) {
      return rv;
    }
    let first = true;
    languages.forEach((language) => {
      rv.pushObject({
        long: language,
        short: language.substring(language.length - 2),
        suffix: first ? '' : `_${language.replace('-', '_')}`,
        lang: language.substring(0, 2),
      });
      first=false;
    });
    return rv;
  }),

  _current_language: null,
  currentLanguage: Ember.computed('langs', '_current_language', {
    get() {
      const translated=this.get('field.translated');
      if (!translated) {
        return '';
      }
      const currentLanguage = this.get('_current_language');
      if (!currentLanguage) {
        const languages = this.get('langs');
        if (languages === undefined || languages.length === 0) {
          return '';
        }
        const rv = languages[0];
        if (rv) {
          return rv.suffix;
        }
        return '';
      }
      return currentLanguage;
    },
    set(key, value) {
      const langs = this.get('langs');
      const lang = langs.filter((l) => {
        return l.long === value;
      });
      let rv = `_${value.replace('-', '_')}`;
      if (lang.length > 0) {
        rv = lang[0].suffix;
        this.set('_current_language', rv);
      }
      return rv;
    }
  }),

  actions: {
    setLang(lang) {
      this.set('currentLanguage', lang);
    },
  },
});
