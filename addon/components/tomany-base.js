import Ember from 'ember';
import BaseCollection from './base-collection';

const {
  computed,
  isEmpty
} = Ember;

export default BaseCollection.extend({
  store: Ember.inject.service(),
  reloadIfFirst: false,
  isFirst: true,

  related: computed('model', 'model.length', 'model.@each.isSoftDeleted', function() {
    const value = this.get('model');
    if (!isEmpty(value)) {
      if (this.get('isFirst') && this.get('reloadIfFirst')) {
        value.forEach((item) => {
          const id = item.get('id');
          if (!Ember.isEmpty(id)) {
            this.get('store').findRecord(value.get('content.type.modelName'), id, {reload: true});
          }
        });
        this.set('isFirst', false);
        this.rerender();
      }
      const rv = value.filter(function(item) {
        return !item.get('isSoftDeleted');
      });
      return rv;
    }
    return value;

  }),

  save() {
    this.get('model').then((model) => {
      model.forEach((record) => {
        if (!record.get('isSoftDeleted')) {
          record.save();
        } else {
          record.destroyRecord();
        }
      });
    });
  },

  delete() {
    this.get('model').then((model) => {
      model.forEach((record) => {
        record.destroyRecord();
      });
    });
  },

  getDefaults() {
    const rv = Ember.copy(this.get('_defaults'));
    if (this.get('field.extra.sortableBy')) {
      rv[this.get('field.extra.sortableBy')] = parseInt(this.get('related.length')) + 1;
    }
    return rv;
  },

  actions: {
    addRelated() {
      const related = this.get('model');
      const record = this.get('store').createRecord(related.get('content.type.modelName'), this.getDefaults());
      related.unshiftObject(record);
    },
    popupAddRelated(model) {
      const related = this.get('model');
      const record = this.get('store').createRecord(model, this.getDefaults());
      related.unshiftObject(record);
      this.addRelated(model, this.get('field'), false, record);
    },
    deleteRelated(record) {
      if (record.get('isNew')) {
        record.rollbackAttributes();
      } else {
        record.set('isSoftDeleted', true);
      }
    }
  },

  // didInsertElement() {
  //   Ember.run.later(this, function() {
  //     if (!this.get('isDestroyed')) {
  //       this.rerender();
  //     }
  //   }, 750);
  // },
});
