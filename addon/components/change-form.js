import Ember from 'ember';
import { task } from 'ember-concurrency';
import layout from '../templates/components/change-form';
import withCustomActionsMixin from '../mixins/with-custom-actions';
import ConfigurableLoaderMixin from '../mixins/configurable-loader';

export default Ember.Component.extend(withCustomActionsMixin, ConfigurableLoaderMixin, {
  layout,
  store: Ember.inject.service(),
  _routing: Ember.inject.service('-routing'),
  flashMessages: Ember.inject.service(),
  inflector: new Ember.Inflector(Ember.Inflector.defaultRules),

  registry: Ember.A(),

  modalOpen: false,
  modalRecord: undefined,
  modalMeta: undefined,
  modalTitle: undefined,
  modelField: undefined,
  isModal: false,
  performing: false,

  defaults: {},
  fieldsets: [{
    title: null,
    fields: [
      {
        name: '__str__',
        label: 'Object',
        read_only: true,
        widget: 'input',
      }
    ]
  }],
  fetch_meta: false,

  _get_defaults_for_fieldset(fields, rv) {
    fields.forEach((field) => {
      const def = Ember.get(field, 'extra.default');
      if (def) {
        rv[Ember.get(field, 'name')] = def;
      }
      const subfields = Ember.get(field, 'fields');
      if (subfields) {
        this._get_defaults_for_fieldset(subfields, rv);
      }
      const tabs = Ember.get(field, 'tabs');
      if (tabs) {
        tabs.forEach((tab) => {
          this._get_defaults_for_fieldset(Ember.get(tab, 'fields'), rv);
        });
      }
    });
  },

  getDefaults() {
    const rv = this.get('defaults');
    if (this.get('sortableBy')) {
      rv[this.get('sortableBy')] = 0;
    }
    const fieldsets = this.get('fieldsets');
    fieldsets.forEach((fieldset) => {
      fieldset = new Ember.Object(fieldset);
      this._get_defaults_for_fieldset(fieldset.get('fields'), rv);
    });
    return rv;
  },

  didReceiveAttrs() {
    if (this.attrs.model !== undefined) {
      return;
    }
    const id = this.get('id');
    if (id !== undefined && id !== -1 && id !== 'new') {
      this.set('model', this.get('store').findRecord(this.get('model_name'), id));
    } else {
      this.set('model', this.get('store').createRecord(
        this.get('model_name'),
        this.getDefaults()
      ));
    }
  },

  register(/* collection */) {
    // console.log('registering', collection);
    // const registry = this.get('registry');
    // const foundAt = registry.indexOf(collection);
    // if (foundAt === -1) {
    //   registry.pushObject(collection);
    // }
    // console.log(this.get('registry'));
  },

  unregister(/* collection */) {
    // console.log('UNregistering', collection);
    // const registry = this.get('registry');
    // this.set('registry', registry.filter((item) => {
    //   return item !== collection;
    // }));
    // console.log(this.get('registry'));
  },

  _do_get_model(value) {
    if (value.hasOwnProperty('isFulfilled')) {
      return value.get('content');
    }
    return value;
  },

  _do_method(record, method, resolve, reject) {
    if (method === 'save') {
      if (record.get('isSoftDeleted') !== true) {
        record[method]().then(resolve, reject);
      } else {
        record.destroyRecord().then(resolve, reject);
      }
    } else {
      record.set('isSoftDeleted', false);
      record[method]().then(resolve, reject);
    }
  },

  _do_for_fields(saved_record, fieldsets, method, top_promises, models) {
    fieldsets.forEach((fieldset) => {
      // fieldset.then((fieldset) => {
      if (fieldset.fields !== undefined) {
        fieldset.fields.forEach((field) => {
          if (field.widget && field.widget.split('-')[0] === 'tomany') {
            top_promises.push(new Ember.RSVP.Promise((resolve, reject) => {
              const promises = [];
              if (saved_record.get(`${field.name}.isFulfilled`)) {
                saved_record.get(field.name).then((related) => {
                  related.forEach((related_record) => {
                    if (models.indexOf(related_record) === -1) {
                      promises.push(
                          this._do_for_all(related_record, [field], method, method === 'save' && field.extra && field.extra.saveTwice).then((saved_related) => {
                            models.push(saved_related);
                          })
                        );
                    }
                  });
                });
              }
              Ember.RSVP.Promise.all(promises)
                  .then(resolve, (err) => { reject(err); });
            }));
          } else if (field.widget && field.widget === 'fieldset') {
            this._do_for_fields(saved_record, [field], method, top_promises, models);
          } else if (field.widget && field.widget === 'tabset') {
            this._do_for_fields(saved_record, field.tabs, method, top_promises, models);
          }
        });
      }
      // });
    });
  },

  _do_for_all(record, fieldsets, method, twice) {
    const top_promises = [];
    const models = [];
    /** TODO: Can probably be improved with ember-concurency */
    const rv = new Ember.RSVP.Promise((resolve_top, reject_top) => {
      let promise;
      if (method === 'rollbackAttributes') {
        promise = new Ember.RSVP.Promise((resolve) => {
          record[method]();
          record.set('isSoftDeleted', false);
          resolve(record);
        });
      } else if (!twice || record.get('isNew')) {
        promise = new Ember.RSVP.Promise((resolve, reject) => {
          if (record.hasOwnProperty('isFulfilled')) {
            record.then((r) => { this._do_method(r, method, resolve, reject); });
          } else {
            this._do_method(record, method, resolve, reject);
          }
        });
      } else {
        promise = Ember.RSVP.resolve(record);
      }
      promise.then((saved_record) => {
        models.push(saved_record);
        this._do_for_fields(saved_record, fieldsets, method, top_promises, models);
        Ember.RSVP.Promise.all(top_promises)
          .then(() => {
            resolve_top(saved_record);
          }, (err) => {
            reject_top(err);
          });
      }, reject_top);
    });
    if (twice) {
      return new Ember.RSVP.Promise((resolve, reject) => {
        rv.then((record) => {
          record[method]().then(resolve, reject);
        }, reject);
      });
    }
    return rv;

  },

  do_redirect(after) {
    const routing = this.get('_routing');
    const model_name = this.get('model_name').split('/');
    if (after === 'redirect') {
      const changeListRoute = this.get('changeListRoute');
      if (changeListRoute === undefined) {
        routing.transitionTo('index');
      } else {
        routing.transitionTo(changeListRoute, model_name);
      }
    } else {
      const id = this.get('model.id');
      if (id !== this.attrs.id) {
        model_name[2] = id;
        routing.transitionTo(routing.get('currentRouteName'), model_name);
      }
    }
  },

  after_save(model, after) {
    this.do_redirect(after);
  },

  after_delete() {
    this.do_redirect('redirect');
  },

  after_cancel() {
    this.do_redirect('redirect');
  },

  save_all: task(function * (after) {
    this.set('performing', after);
    const promise = this._do_for_all(this.get('model'), this.get('fieldsets'), 'save', this.get('saveTwice')).then((model) => {
      this.get('flashMessages').success(`${this.get('model.__str__')} was sucessfully saved`);
      this.after_save(model, after);
    }, (err) => {
      this.get('flashMessages').danger('There was an error while saving this record');
      /* eslint-disable no-console */
      console.error(err);
      /* eslint-enable no-console */
    }).finally(() => {
      this.set('performing', false);
    });
    yield promise;
  }),

  cancel_all: task(function * (model) {
    this.set('performing', 'cancel');
    if (model.hasOwnProperty('isFulfilled')) {
      yield model.then((m) => {
        this._do_for_all(m, this.get('fieldsets'), 'rollbackAttributes').then(() => {
          this.after_cancel();
        }).catch((err) => {
          this.get('flashMessages').danger('There was an error rolling back your changes, please try to reload the page');
          /* eslint-disable no-console */
          console.error(err);
          /* eslint-enable no-console */
        }).finally(() => {
          this.set('performing', false);
        });
      });
    } else {
      yield this._do_for_all(model, this.get('fieldsets'), 'rollbackAttributes').then(() => {
        this.after_cancel();
      }).catch((err) => {
        this.get('flashMessages').danger('There was an error rolling back your changes, please try to reload the page');
        /* eslint-disable no-console */
        console.error(err);
        /* eslint-enable no-console */
      }).finally(() => {
        this.set('performing', false);
      });
    }
  }),

  cancel_related: task(function * () {
    const field = this.get('modalField');
    if (field) {
      const model = this._do_get_model(this.get('modalRecord'));
      const fieldsets = this.get('modalMeta.fieldsets');
      this.set('performing', 'cancel_related');
      yield this._do_for_all(model, fieldsets, 'rollbackAttributes').then(() => {
        this.set('modalOpen', false);
        this.set('modalMeta', undefined);
        this.set('modalTitle', undefined);
        this.set('modelField', undefined);
      }).catch((err) => {
      /* eslint-disable no-console */
        console.error(err);
      /* eslint-enable no-console */
      }).finally(() => {
        this.set('performing', false);
      });
    } else {
      this.set('modalOpen', false);
      this.set('modalMeta', undefined);
      this.set('modalTitle', undefined);
      this.set('modelField', undefined);
    }
  }),

  save_related: task(function * () {
    this.set('performing', 'save_related');
    const model = this._do_get_model(this.get('modalRecord'));
    const field = this.get('modalField');
    const fieldsets = this.get('modalMeta.fieldsets');
    yield this._do_for_all(model, fieldsets, 'save').then(() => {
      if (this.get('modalToMany')) {
        field.get('value').pushObject(model);
      } else {
        field.set('value', model);
      }
      this.set('modalOpen', false);
      this.set('modalMeta', undefined);
      this.set('modalTitle', undefined);
      this.set('modelField', undefined);
      this.get('flashMessages').success(`${model.get('__str__')} was sucessfully saved`);
    }).catch((err) => {
      this.get('flashMessages').danger('There was an error while saving this record');
      /* eslint-disable no-console */
      console.error(err);
      /* eslint-enable no-console */
    }).finally(() => {
      this.set('performing', false);
    });
  }),

  confirm_related: task(function * () {
    this.set('performing', 'confirm_related');
    const model = this._do_get_model(this.get('modalRecord'));
    const fieldsets = this.get('fieldsets');
    const recordName = model.get('__str__');
    yield this._do_for_all(model, fieldsets, 'destroyRecord').then(() => {
      this.set('modalOpen', true);
      this.set('modalMessage', false);
      this.set('modalMeta', undefined);
      this.set('modalTitle', undefined);
      this.set('modelField', undefined);
      this.get('flashMessages').success(`${recordName} was sucessfully deleted`);
      this.after_delete();
    }).catch((err) => {
      this.get('flashMessages').danger(`${recordName} could not be deleted`);
      /* eslint-disable no-console */
      console.error(err);
      /* eslint-enable no-console */
    }).finally(() => {
      this.set('performing', false);
    });
  }),

  buttonsDisabled: Ember.computed.or('save_all.isRunning', 'cancel_all.isRunning', 'modalOpen', 'actionIsRunning'),
  modalButtonsDisabled: Ember.computed.or('save_related.isRunning', 'confirm_related.isRunning', 'cancel_related.isRunning', 'actionIsRunning'),

  actions: {

    add_related(model_name, field, toMany, record) {
      if (record === undefined) {
        record = this.get('store').createRecord(model_name, {});
      }
      this.set('modalRecord', record);
      this.set('modalField', field);
      this.set('modalMessage', false);
      const inflector = this.get('inflector');
      const parts = model_name.split('/');
      this.set('modalTitle', `Add ${parts[1]}`);
      parts[1] = inflector.pluralize(parts[1]);
      const modelLoader = this.get('modelLoader');
      modelLoader.get_model_meta(parts[0], parts[1]).then((meta) => {
        this.set('modalMeta', meta);
        this.set('modalToMany', toMany);
        this.set('modalOpen', true);
      });
    },

    edit_related(model_name, field, isRecord) {
      let record;
      if (isRecord) {
        record = field;
      } else {
        record = field.get('value');
      }
      this.set('modalRecord', record);
      this.set('modalField', field);
      this.set('modalMessage', false);
      const inflector = this.get('inflector');
      const parts = model_name.split('/');
      this.set('modalTitle', `Edit ${parts[1]} ${record.get('__str__')}`);
      parts[1] = inflector.pluralize(parts[1]);
      const modelLoader = this.get('modelLoader');
      modelLoader.get_model_meta(parts[0], parts[1]).then((meta) => {
        this.set('modalMeta', meta);
        this.set('modalOpen', true);
      });
    },

    delete_all() {
      const record = this.get('model');
      const model_name = this.get('model_name');
      const parts = model_name.split('/');
      this.set('modalField', false);
      this.set('modalRecord', record);
      this.set('modalTitle', `Confirm delete ${parts[1]} ${record.get('__str__')}`);
      this.set('modalMessage', `Please confirm you want to delete ${record.get('__str__')}`);
      this.set('modalOpen', true);
    },
  }

});
