import TextAreaComponent from './form-textarea';
import layout from '../templates/components/form-markdown';

export default TextAreaComponent.extend({
  layout,
  type: 'markdown',
  classNames: ['markdown', ],
});
