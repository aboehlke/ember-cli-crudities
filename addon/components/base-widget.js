import Ember from 'ember';
import layout from '../templates/components/base-widget';
import BoundValueMixin from '../mixins/boundvalue';

export default Ember.Component.extend(BoundValueMixin, {
  layout,

  classNames: ['form-group'],
  classNameBindings: ['field.translated:translated', 'hasError', 'required', 'readonly'],

  label: null,
  type: 'text',
  horiClass: 'col-sm-3',
  inputClass: 'col-sm-9',

  isInput: Ember.computed('type', function() {
    const type = this.get('type');
    if (-1 !== ['text', 'tel', 'email', 'number', 'date', 'time', 'datetime', 'color', 'month', 'week', 'url'].indexOf(type)) {
      return true;
    }
    return false;
  }),

  isDateTime: Ember.computed('type', function() {
    const type = this.get('type');
    if (-1 !== ['date', 'time', 'datetime', 'month', 'week'].indexOf(type)) {
      return true;
    }
    return false;
  }),

  momentFormat: Ember.computed('type', function() {
    const type = this.get('type');
    if (type === 'date') {
      return 'ddd YYYY-MM-DD';
    }
    if (type === 'time') {
      return 'kk:mm:ss';
    }
    return 'ddd YYYY-MM-DD HH:mm:ss zz';
  }),

  hasLabel: Ember.computed('label', 'formLayout', 'type', function() {
    const formLayout = this.get('formLayout');
    const label = this.get('label');
    const type = this.get('type');
    return !Ember.isEmpty(label) && formLayout === 'horizontal' && type !== 'checkbox';
  }),

  bsInputClass: Ember.computed('inputClass', 'hasLabel', function() {
    const hasLabel = this.get('hasLabel');
    if (!hasLabel) {
      return 'col-xs-12';
    }
    return this.get('inputClass');
  }),

  status: Ember.computed('model.errors.@each.length', function() {
    if (this.get('model.errors.' + this.get('property') + '.length')) {
      return 'error';
    }
    return 'success';

  }),

  hasError: Ember.computed.equal('status', 'error'),
});
