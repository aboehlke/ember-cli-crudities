import Ember from 'ember';
import layout from '../templates/components/form-foreignkey';
import ForeignKeyBase from './foreignkey-base';
import BoundValueMixin from '../mixins/boundvalue';

export default ForeignKeyBase.extend(BoundValueMixin, {
  layout,
  type: 'foreignkey',
  isExpanded: false,

  _placeholder: '---',
  placeHolder: Ember.computed.or('placeholder', '_placeholder'),
});
