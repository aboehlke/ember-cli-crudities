import Ember from 'ember';
import layout from '../templates/components/tomany-lines';

export default Ember.Component.extend({
  layout,
  tagName: 'table',
  classNames: ['table', 'table-condensed', 'table-striped', 'tomany'],

  reorderAction: 'reorderItems',

  onLengthChange: Ember.observer('related.length', function() {
    Ember.run.later(this, () => {
      this.resize();
    }, 150);
  }),

  resize: function() {
    this.$('tr').css('display', 'block');
    let width = 0;
    const padderColspan = parseInt(this.get('padderColspan'));
    const labelsRow = !!this.get('labelsRow');
    let header_offset = 1;
    if (padderColspan > 1) {
      header_offset = 2 - padderColspan;
    }

    for(let i=padderColspan; i<this.get('column_count'); i++) {
      width = Math.max(
        Math.max.apply(
          Math,
          this.$(`tbody tr td:nth-child(${i + 1})`).map(
            function() {
              return window.$(this).innerWidth();
            }).get()
        ),
        labelsRow ? this.$(`thead tr.headers th:nth-child(${i + header_offset})`).innerWidth() : 0
      );
      this.$(`tbody tr td:nth-child(${i + 1})`).css('min-width' , width);
      if (labelsRow) {
        this.$(`thead tr.headers th:nth-child(${i + header_offset})`).css('min-width', width);
      }
    }
    if (padderColspan > 0 && labelsRow) {
      width = 0;
      for (let j=0; j<padderColspan; j++) {
        width += this.$(`tbody tr td:nth-child(${j+1})`).innerWidth();
      }
      this.$('thead tr.headers th:nth-child(1)').css('min-width', width);
    }
    this.$('tr').css('display', 'flex');
  },

  didInsertElement() {
    this._super();
    Ember.run.later(this, () => {
      this.resize();
    }, 150);
    // window.$(window).on('resize', this.resize.bind(this));
    // window.$(window).on('orientationchange', this.resize.bind(this));
  },

  willDestroy() {
    // window.$(window).off('resize', this.resize.bind(this));
    // window.$(window).off('orientationchange', this.resize.bind(this));
  },

  actions: {
    reorderItems(items) {
      this.sendAction('reorderAction', items);
    }
  }

});
