import Ember from 'ember';
import layout from '../templates/components/action-wizard';
import { task } from 'ember-concurrency';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  flashMessages: Ember.inject.service(),
  layout,
  tetherClass: 'modal-dialog modal-lg',
  tetherAttachment: 'middle center',
  tetherTargetAttachment: 'middle center',
  tetherTarget: 'document.body',

  doRqst: task(function * (url, act, model) {
    yield this.wizard_action(url, act, model).then(() => {
      this.overlayClick();
    });
  }),

  buttonsDisabled: Ember.computed.alias('doRqst.isRunning'),

  actions: {
    cancel() {
      this.overlayClick();
    },
  }
});
