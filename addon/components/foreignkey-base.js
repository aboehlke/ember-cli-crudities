import Ember from 'ember';

export default Ember.Component.extend({
  tagName: null,

  store: Ember.inject.service(),

  autoComplete: false,
  _label: '__str__',
  choiceLabel: Ember.computed.or('extra.label', '_label'),
  _search: 'backend',
  search: Ember.computed.or('extra.search', '_search'),

  choices: Ember.computed('content', 'content.@each.isSoftDeleted', function() {
    const content = this.get('content');
    const values = new Ember.A();
    if (content) {
      content.map((item) => {
        if (!item.get('isSoftDeleted')) {
          values.pushObject(item);
        }
      });
    }
    return values;
  }),

  init() {
    this._super();
    if (!this.get('readonly')) {
      const property = this.get('extra.property_path');
      let rv;
      if (property !== undefined) {
        this.set('content', this.get(`model.${property}`));
        return;
      } else if (this.get('extra.dontSeed')) {
        rv = new Ember.A();
      } else {
        const model_name = this.get('extra.related_model');
        const query = this.get('extra.query');
        if (query !== undefined) {
          rv = this.get('store').query(model_name, query);
        } else {
          rv = this.get('store').findAll(model_name);
        }
      }
      this.set('_content', rv);
      this.set('content', rv);
    }
  },

  didInsertElement() {
    const model = this.get('model');
    const property = this.get('extra.property_path');
    if (property) {
      model.addObserver(property, this._do_search.bind(this, undefined));
    }
    const filters = this.get('extra.dynamicFilters');
    if (filters) {
      filters.forEach((filter) => {
        model.addObserver(filter.property_path, this._do_search.bind(this, undefined));
      });
    }
  },

  willDestroyElement() {
    const model = this.get('model');
    const property = this.get('extra.property_path');
    if (property) {
      model.removeObserver(property, this._do_search.bind(this, undefined));
    }
    const filters = this.get('extra.dynamicFilters');
    if (filters) {
      filters.forEach((filter) => {
        model.removeObserver(filter.property_path, this._do_search.bind(this, undefined));
      });
    }
  },

  _do_search(term) {
    const property = this.get('extra.property_path');
    let content = Ember.A();
    if (property !== undefined) {
      content = this.get(`model.${property}`);
      if (content !== undefined && term) {
        content = content.filter((item) => {
          const str = item.get(this.get('choiceLabel'));
          if (str) {
            return str.toLowerCase().indexOf(term.toLowerCase()) > -1;
          }
          return false;
        });
      } else if (content === undefined) {
        content = new Ember.A();
      }
    } else if (this.get('search') === 'simple') {
      content = this.get('_content');
      if (term) {
        content = content.filter((item) => {
          const str = item.get(this.get('choiceLabel'));
          if (str) {
            return str.toLowerCase().indexOf(term.toLowerCase()) > -1;
          }
          return false;
        });
      }
    } else  {
      const model_name = this.get('extra.related_model');
      let query = this.get('extra.query');
      if (query === undefined) {
        query = {};
      }
      query['search'] = term;
      const filters = this.get('extra.dynamicFilters');
      if (filters) {
        filters.forEach((filter) => {
          const value = this.get(`model.${filter.property_path}`);
          if (value !== undefined) {
            query[filter.filter] = value;
          }
        });
      }
      content = this.get('store').query(model_name, query);
    }
    if (this.get('search') === 'simple' || property !== undefined) {
      const query = this.get('extra.query');
      let deffered = false;
      if (query) {
        for (const prop in query) {
          if (query.hasOwnProperty(prop)) {
            content = content.filter((item) => {
              if (!item.get('isLoaded')) {
                if (!deffered) {
                  Ember.run.later(this._do_search.bind(this, term), 150);
                  deffered = true;
                }
                return true;
              }
              const val = item.get(prop);
              return val === query[prop];
            });
          }
        }
      }
      const filters =this.get('extra.dynamicFilters');
      if (filters) {
        filters.forEach((filter) => {
          const val = this.get(`model.${filter.property_path}`);
          if (val !== undefined) {
            content = content.filter((item) => {
              if (!item.get('isLoaded')) {
                if (!deffered) {
                  Ember.run.later(this._do_search.bind(this, term), 150);
                  deffered = true;
                }
                return true;
              }
              return item.get(filter.remote_property_path) === val;
            });
          }
        });
      }
    }
    // content = content.filterBy('isSoftDeleted', false);
    this.set('content', content);
    return content;
  },

  linkApp: Ember.computed('extra', 'extra.related_model', function() {
    const related = this.get('extra.related_model');
    return related.split('/').shift();
  }),

  linkModel: Ember.computed('extra', 'extra.related_model', function() {
    const related = this.get('extra.related_model');
    return related.split('/').pop();
  }),

  actions: {
    change(value) {
      this.set('value', value);
    },
    search(term) {
      return this._do_search(term);
    }
  }
});
