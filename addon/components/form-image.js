import Ember from 'ember';
import FormFileComponent from './form-file';
import layout from '../templates/components/form-image';

export default FormFileComponent.extend({
  layout,
  type: 'image',
  _width: '175px',
  _height: '175px',
  _tn_width: '75px',
  _tn_height: '75px',

  width: Ember.computed.or('extra.width', '_width'),
  height: Ember.computed.or('extra.height', '_height'),

  tnWidth: Ember.computed.or('extra.tnWidth', '_tn_width'),
  tnHeight: Ember.computed.or('extra.tnHeight', '_tn_height'),

  background: Ember.computed.or('value', 'extra.placeholder'),

  style: Ember.computed('width', 'height', 'background', function() {
    const width = this.get('width');
    const height = this.get('height');

    let rv = `border: 1px solid #999; display: inline-block; width: ${width}; height:${height};`;

    const image = this.get('background');
    if (image) {
      rv += ` position: relative; background-repeat: no-repeat; background-position: center center; background-size: contain; background-image: url(${image})`;
    } else {
      rv += ' display: flex; justify-content: center; align-items: center;';
    }

    return rv;
  }),

  tnStyle: Ember.computed('tnWidth', 'tnHeight', function() {
    const width = this.get('tnWidth');
    const height = this.get('tnHeight');
    return `max-width: ${width}; max-height: ${height};`;
  })
});
