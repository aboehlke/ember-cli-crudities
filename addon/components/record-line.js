import Ember from 'ember';
import layout from '../templates/components/record-line';
import SortableItem from 'ember-sortable/mixins/sortable-item';
import withCustomActionsMixin from '../mixins/with-custom-actions';
import conditionalFormat from '../utils/conditional-format';

export default Ember.Component.extend(SortableItem, withCustomActionsMixin, {
  layout,
  tagName: 'tr',
  classNameBindings: ['state', 'conditionalFormatting'],
  timer: undefined,
  handle: '#none',

  state: '',
  conditionalFormatting: Ember.computed(function() {
    const model = this.get('model');
    const conditions = this.get('conditionalFormat');
    return conditionalFormat(model, conditions);
  }).volatile(),

  didReceiveAttrs() {
    if (this.attrs.parent !== undefined) {
      const fields = ['display_fields', 'innerClass', 'changeFormRoute', 'app_name', 'label',
        'orderable', 'custom_actions'];
      fields.forEach((field) => {
        if (this.attrs[field] === undefined) {
          this.set(field, this.get(`parent.${field}`));
        }
      });
    }
  },

  actions: {
    addNew() {

    },
    save(/* model, field */) {
      Ember.run.debounce(this, () => {
        this.get('model').save().then(() => {
          if (this.timer !== undefined) {
            Ember.run.cancel(this.timer);
          }
          this.set('state', 'success');
          this.timer = Ember.run.later(this, () => {
            this.set('state', '');
          }, 750);
        }).catch((err) => {
          /* eslint-disable no-console */
          console.error(err);
          /* eslint-enable no-console */
          if (this.timer !== undefined) {
            Ember.run.cancel(this.timer);
          }
          this.set('state', 'danger');
          this.timer = Ember.run.later(this, () => {
            this.set('state', '');
          }, 750);
        });
      }, 250);
    },
  }
});
