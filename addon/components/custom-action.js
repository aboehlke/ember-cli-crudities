import Ember from 'ember';
import layout from '../templates/components/custom-action';

export default Ember.Component.extend({
  layout,
  isMethod: Ember.computed('act.type', function() {
    const type = this.get('act.type');
    return type === 'modelMethod' || type === 'closureMethod';
  }),
});
