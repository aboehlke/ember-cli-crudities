import Ember from 'ember';
import fetch from 'ember-network/fetch';
import { task } from 'ember-concurrency';
import Cookies from 'ember-cli-js-cookie';
import ConfigurableLoader from './configurable-loader';

export default Ember.Mixin.create(ConfigurableLoader, {
  store: Ember.inject.service(),
  modelLoader: Ember.inject.service(),
  flashMessages: Ember.inject.service(),
  receiver: this,
  main: undefined,
  tetherClass: 'modal-dialog modal-lg',
  tetherAttachment: 'middle center',
  tetherTargetAttachment: 'middle center',
  tetherTarget: 'document.body',

  _do_meth(act, receiver, model) {
    const { type, method, text } = act;
    if (type === 'modelMethod') {
      if (model.hasOwnProperty('isFulfilled')) {
        return new Ember.RSVP.Promise((resolve, reject) => {
          model.then((m) => {
            resolve(m[method](act));
            //.then(resolve).catch(reject);
          }).catch((err) => {
            this.get('flashMessages').danger(`An error occured while performing "${text}" on ${this.get('model.__str__')}`);
            reject(err);
          });
        });
      }
      return model[method](act).then(() => {
        let receiver = this.get('receiver');
        if (!receiver) {
          receiver = this;
        }
        if (act.refresh_list) {
          try {
            receiver.filterModel(receiver.get('_search_value'), receiver.get('_filter_values'), 1);
          } catch (e) {
            console.error(e);
          }
        }
      });

    }
    return receiver[method](model, act);

  },

  actionIsRunning: Ember.computed.or('method.isRunning', 'rqst.isRunning'),

  method: task(function * (act, receiver, model) {
    const { text } = act;
    this.set('performing', text);
    yield this._do_meth(act, receiver, model);
    // .finally(() => {
    this.set('performing', false);
    // });
  }),

  doRqst(url, act, model) {
    const { verb, pushPayload, text } = act;
    let postfix = '';
    const params = {
      method: verb,
      headers: {
        'X-CSRFToken': Cookies.get('csrftoken'),
        'Content-Type': 'application/json',
      }, credentials: 'include'
    };
    if (act.atOnce === true) {
      postfix = `&${model}`;
    } else if (model !== undefined && verb.toUpperCase() === 'POST') {
      params['body'] = JSON.stringify(model.serialize());
    }
    const rv = new Ember.RSVP.Promise((resolve, reject) => {
      fetch(`${url}?format=json${postfix}`, params).then((response) => {
        if (response.status === 400) {
          response.json().then((json) => {
            reject(json);
          }).catch(() => {
            reject(response);
          });
        } else if (response.status > 400) {
          reject(response);
        } else if (response.status === 204) {
          resolve({});
        } else {
          response.json().then((json) => {
            resolve(json);
          }).catch(() => {
            reject(response);
          });
        }
      });
    });
    if (pushPayload) {
      return new Ember.RSVP.Promise((resolve, reject) => {
        rv.then((json) => {
          this.get('store').pushPayload(json);
          resolve();
        }, (err) => {
          this.get('flashMessages').danger(`An error occured while performing "${text}" on ${this.get('model.__str__')}`);
          reject(err);
        });
      });
    } else if (act.refresh === true) {
      return new Ember.RSVP.Promise((resolve, reject) => {
        rv.then((/* json */) => {
          this.filterModel(this.get('_search_value'), this.get('_filter_values'), 1).then(() => {
            resolve();
          }, reject);
        }, reject);
      });
    }
    return rv.catch((/* err */) => {
      this.get('flashMessages').danger(`An error occured while performing "${text}" on ${this.get('model.__str__')}`);
    });

  },

  rqst: task(function * (url, act, model) {
    const { text } = act;
    this.set('performing', text);
    yield this.doRqst(url, act, model).finally(() => {
      this.set('performing', false);
    });
  }),

  _set_defaults_for_wizard(model, fieldsets) {
    const fields = Ember.get(fieldsets, 'fields');
    if (!fields) {
      return;
    }
    fields.forEach((field) => {
      if (field.hasOwnProperty('fields')) {
        this._set_defaults_for_wizard(model, field);
      } else if (field.hasOwnProperty('tabs')) {
        field.tabs.forEach((tab) => {
          this._set_defaults_for_wizard(model, tab);
        });
      } else {
        model.set(Ember.get(field, 'name'), Ember.get(field, 'extra.default'));
        const prop = Ember.get(field, 'extra.default_property_path');
        if (prop) {
          model.set(Ember.get(field, 'name'), model.get(prop));
        }
      }
    });
  },

  _wizard(record, act) {
    const modelLoader = this.get('modelLoader');
    const promises = [];
    const { params } = act;
    let main = this.get('main');
    if (main === undefined) {
      main = this;
    }
    params.needs.forEach((need) => {
      promises.push(modelLoader.ensure_model(need.app, need.singular, need.plural));
    });

    promises.push(modelLoader.ensure_model('wizard', params.model, params.model, false));
    Ember.RSVP.all(promises).then(() => {
      main.set('wizardParams', [modelLoader.merge_fields_fieldset(params.fields, {fields: Ember.get(params, 'fieldsets')})]);
      const model =  this.get('store').createRecord(`wizard/${params.model}`, {});
      model.set('parent', record);
      this._set_defaults_for_wizard(model, {'fields': Ember.get(params, 'fieldsets')});
      main.set('wizardModel', model);
      main.set('wizardAct', act);
      main.set('wizardOpen', true);
      main.set('wizard_action', this.doRqst);
    }, (err) => {
      console.error(err);
      this.get('flashMessages').danger(`An error occured while performing "${act.text}" on ${this.get('model.__str__')}`);
    });
  }
});
