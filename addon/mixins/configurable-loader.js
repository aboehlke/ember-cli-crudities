import Ember from 'ember';

export default Ember.Mixin.create({
  init() {
    this._super();
    const appConfig = Ember.getOwner(this).resolveRegistration('config:environment');
    const modelLoader = Ember.get(appConfig, 'ember-cli-crudities.model-loader');
    if (modelLoader !== undefined) {
      this.set('modelLoader', Ember.inject.service(modelLoader));
    }
  }
});
