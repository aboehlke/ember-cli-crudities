import Ember from 'ember';
import evaluate from '../utils/evaluator';

export default Ember.Mixin.create({
  evaluate(condition, value) {
    return evaluate(condition, value);
  }
});
