import Ember from 'ember';

export function actionUrl(params/*, hash*/) {
  return params[0].replace(':id', params[1]);
}

export default Ember.Helper.helper(actionUrl);
