import Ember from 'ember';
import evaluate from '../utils/evaluator';

export function evaluateCondition(params/*, hash*/) {
  const condition = params[0];
  const model = params[1];
  if (condition === undefined) {
    return false;
  }

  if (model === undefined) {
    return false;
  }
  const value = model.get(condition.property_path);
  const rv = evaluate(condition, value);
  return rv;
}

export default Ember.Helper.helper(evaluateCondition);
